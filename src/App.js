import Card from './components/Card';
import SearchTable from './components/SearchTable';
import React, { useState } from 'react';

const App = () => {
  const data = [
    {
      id: "1",
      key: "POS_newsite_launching",
      processID: "1945311",
      flowName: "فرآیند امکان سنجی و راه اندازی کاندید سایت /تست کامران -رضا",
      currentTaskName: "کنترل حقوقی پیش قرارداد سایت",
      processState: {
        key: "COMPLETED"
      },
      variables: {
        relation_employee_name: "آقای کامران گیلک",
        relation_name: "کامران",
        relation_priority: "important",
      },
      processStartTime: "1402/10/02 10:15:26",
      processEndTime: "1402/10/05 10:15:26",
      details: {
        title: "آپادانا",
        priority: "P1",
        id: "000291425",
        state: "INITIAL",
        floor: 5,
        code: "52",
        ring: {
          name: "Ring 1"
        }
      }
    },
    {
      id: "2",
      key: "POS_tower_feasibility",
      processID: "1945312",
      flowName: "امکان‌سنجی و راه‌اندازی سرویس برج و مجتمع",
      currentTaskName: "آماده‌سازی اقلام مصرفی",
      processState: {
        key: "COMPLETED"
      },
      variables: {
        relation_employee_name: "آقای کامران گیلک",
      },
      processStartTime: "1402/10/02 10:15:26",
      details: {
        title: "آپادانا2",
        priority: "P2",
        id: "000291426",
        state: "CONFIRM",
        floor: 5,
        code: "52",
      }
    },
    {
      id: "3",
      key: "POS_tower_feasibility",
      processID: "1945313",
      flowName: "امکان‌سنجی و راه‌اندازی سرویس برج و مجتمع/شناسه محل ارایه سرویس",
      currentTaskName: "",
      processState: {
        key: "EXTERNALLY_TERMINATED"
      },
      variables: {
        relation_employee_name: "شهاب لقایی",
        relation_name: "مشتری اول",
        relation_priority: "normal",
      },
      processEndTime: "1402/10/05 10:15:26",
      details: {
        title: "آپادانا۳",
        priority: "P3",
        id: "000291427",
        state: "CONFIRM",
        floor: 5,
        code: "52",
        ring: {}
      }
    },
    {
      id: "4",
      key: "POS_tower_feasibility",
      processID: "1945314",
      flowName: "امکان‌سنجی و راه‌اندازی سرویس برج و مجتمع/سایت الف",
      currentTaskName: "امکان‌سنجی لینک PTP",
      processState: {
        key: "ACTIVE"
      },
      variables: {},
      processStartTime: "1402/10/02 10:15:26",
      processEndTime: "1402/10/05 10:15:26",
      details: {
        title: "آپادانا4",
        priority: "P4",
        id: "0002914244",
        state: "INITIAL",
        floor: 54,
        code: "44",
        ring: {
          name: "Ring 4"
        }
      }
    },
    {
      id: "5",
      key: "POS_newsite_launching",
      processID: "1945315",
      flowName: "فرآیند امکان سنجی و راه اندازی کاندید سایت /ساختمان دلتا",
      currentTaskName: "نتیجه مذاکره با مالک/ اپراتور",
      processState: {
        key: "ACTIVE"
      },
      variables: {
        relation_employee_name: "راهبر سیستم",
        relation_priority: "normal",
      },
      details: {
        title: "آپادانا5",
        priority: "P5",
        id: "00029142555",
        state: "INITIAL",
        floor: 55,
        code: "55",
        ring: {
          name: "Ring 5"
        }
      }
    },
    {
      id: "6",
      key: "ca_collect_first_place",
      processID: "1945316",
      flowName: "جمع‌آوری",
      currentTaskName: "مشخصات صحیح سیم‌کارت",
      processState: {
        key: "EXTERNALLY_TERMINATED"
      },
      variables: {
        relation_employee_name: "رضا حسینی",
        relation_name: "خانم نورا هاشمی",
        relation_priority: "important",
      },
      details: {
        title: "آپادانا6",
        priority: "P6",
        id: "000291466",
        state: "INITIAL",
        floor: 66,
        code: "66",
      }
    },
  ];
  
  const flows = [
  {
    value: "POS_newsite_launching",
    label: "فرآیند امکان سنجی و راه اندازی کاندید سایت"
  },
  {
    value: "POS_tower_feasibility",
    label: "امکان‌سنجی و راه‌اندازی سرویس برج و مجتمع"
  },
  {
    value: "ca_collect_first_place",
    label: "جمع‌آوری"
  },
  ];
  
  const searchItems = [
  {
    type: 'input',
    name: 'processID',
    label: 'شناسه فرآیند'
  },
  {
    type: 'select',
    options: ['', ...flows.map(flow => flow.label)],
    name: 'flowName',
    label: 'نام فرآیند'
  },
  {
    type: 'select',
    options: ['', ...new Set(data.map(item => item.variables.relation_employee_name).filter(name => name))],
    name: 'relationEmployeeName',
    label: 'شخص مسئول'
  }
  ];
  const columns = [
  {
  headerTitle: "شناسه فرآیند",
  dataFieldName: "processID",
  render: (rowData) => (
    <a href="">{rowData.processID}</a>
  )
  },
  {
  headerTitle: "نام فرآیند",
  dataFieldName: "flowName"
  },
  {
  headerTitle: "موقعیت جاری",
  dataFieldName: "currentTaskName"
  },
  {
  headerTitle: "شخص مسئول",
  render: (rowData) => rowData.variables.relation_employee_name || '_',
  },
  {
  headerTitle: "مشتری/متقاضی",
  render: (rowData) => rowData.variables.relation_name || "_",
  
  },
  {
  headerTitle: "زمان شروع فرآیند",
  dataFieldName: "processStartTime"
  },
  {
  headerTitle: "زمان پابان فرآیند",
  dataFieldName: "processEndTime"
  },
  {
  headerTitle: "وضعیت فرآیند",
  render: (rowData) => {
    const processStateKey = rowData.processState?.key;
    const processState = processStates.find(state => state.key === processStateKey);
    const processStateValue = processState ? processState.value : '';
    const processStateClass = processState ? `state state-${processStateKey.toLowerCase().replace(/[\s_]/g, '-')}` : '';  
    const priorityKey = rowData.variables?.relation_priority;
    const priority = priorities.find(priority => priority.key === priorityKey);
    const priorityValue = priority ? priority.value : ' ';
    const priorityClass = priority ? `priority priority-${priorityKey.toLowerCase().replace(/\s/g, '-')}` : '';
  
    return (
      <div className="two-states">
        <div className={processStateClass}>
          {processStateValue}
        </div>
        <div className={priorityClass}>
          {priorityValue}
        </div>
      </div>
    );
  },
  },
  {
  headerTitle: "عملیات",
  render: (rowData) => {
    return (
      <div className="more">
        <span className="icon-ellipsis-v"></span>
        <div className="opt">
          <ul>
            <li><a href="">مشاهده</a></li>
          </ul>
        </div>
      </div>
    );
  },
  }
  ];
  
  const processStates = [
  {
    key: "ACTIVE",
    value: "در حال پیگیری"
  },
  {
    key: "COMPLETED",
    value: "خاتمه یافته"
  },
  {
    key: "EXTERNALLY_TERMINATED",
    value: "بسته‌شده"
  },
  ];
  
  const priorities = [
  {
    key: "important",
    value: "مهم"
  },
  {
    key: "normal",
    value: "معمولی"
  },
  ];
  
  const initialSearchState = Object.fromEntries(searchItems.map(item => [item.name, '']));
  const [searchCriteria, setSearchCriteria] = useState(initialSearchState);
  
  const handleReset = () => {
    setSearchCriteria(initialSearchState);
  };

 const filteredData = data.filter(item => {
        return searchItems.every(searchItem => {
          if (searchItem.type === 'input' || searchItem.type === 'select') {
            let itemValue;
            if (searchItem.name === 'relationEmployeeName') {
              // Access 'relation_employee_name' from the 'variables' object
              itemValue = item.variables ? item.variables.relation_employee_name : '';
            } 
            else {
              // Access other fields directly from the item
              itemValue = item[searchItem.name];
            }
            const searchCriteriaValue = searchCriteria[searchItem.name];
                
            // Comparison logic to include null check and partial matching
            if (searchCriteriaValue) {
              return itemValue && itemValue.toLowerCase().includes(searchCriteriaValue.toLowerCase());
            } 
            else {
              return true; // If search criteria is empty, consider it a match
            }
          } 
          else {
            return true; // For unsupported field types, always return true
          }
        });
  });

  const [showRowColumn, setShowRowColumn] = useState(false);

  const handleCheckboxChange = (event) => {
    setShowRowColumn(event.target.checked);
  };
  
  const tableColumns = showRowColumn
    ? [
        { headerTitle: 'ردیف', render: (_, index) => String(index + 1) },
        ...columns
      ]
    : columns;

  return (
    <div className="main-list">
      <Card
        title="گزارش فرآیندها"
        hasBackButton
        extra={<button className='extra'>extra</button>}
      >
        
        <div className='row-checkbox'>
          <label>
            <input type="checkbox" checked={showRowColumn} onChange={handleCheckboxChange} />
            نمایش ردیف
          </label>
        </div>

        <SearchTable
          searchboxItems={searchItems}
          onSearch={setSearchCriteria}
          onReset={handleReset}
          columns={tableColumns}
          data={filteredData}
        />
      </Card>
    </div>
  );
};

export default App;