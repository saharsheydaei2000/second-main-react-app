import React from 'react';

const Card = (props) => {
    return (
      <div className="card">
        <div className="card-header">
          {props.title}
          {props.extra}
          {props.hasBackButton && (
            <button className="icon">
              <span className="icon-undo"></span>
            </button>
          )}
        </div>
        <div className="card-content">{props.children}</div>
      </div>
    );
  };
export default Card;