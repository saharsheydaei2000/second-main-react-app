import React from 'react';

const SearchForm = ({ searchboxItems, onSearch, onReset }) => {
  const [searchCriteria, setSearchCriteria] = React.useState(
    Object.fromEntries(searchboxItems.map(item => [item.name, '']))
  );

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setSearchCriteria(prevCriteria => ({
      ...prevCriteria,
      [name]: value
    }));
  };

  const handleSearch = () => {
    onSearch(searchCriteria);
  };

  const handleReset = () => {
    setSearchCriteria(Object.fromEntries(searchboxItems.map(item => [item.name, ''])));
    onReset();
  };

  return (
    <div className="search-form">
      {searchboxItems.map((item, index) => {
        if (item.type === 'input') {
          return (
            <div key={index}>
              <label>{item.label}</label>
              <input
                type="text"
                name={item.name}
                value={searchCriteria[item.name]}
                onChange={handleInputChange}
              />
            </div>
          );
        } else if (item.type === 'select' && Array.isArray(item.options)) {
          return (
            <div key={index}>
              <label>{item.label}</label>
              <select
                name={item.name}
                value={searchCriteria[item.name]}
                onChange={handleInputChange}
              >
                {item.options.map((option, optionIndex) => (
                  <option key={optionIndex} value={option}>
                    {option}
                  </option>
                ))}
              </select>
            </div>
          );
        } else {
          return null; // Unsupported field type or missing options
        }
      })}
      <div className="buttons-row">
        <div className="recovery-btn">
          <button onClick={handleReset}>بازنشانی</button>
        </div>
        <div className="search-btn">
          <button onClick={handleSearch}>جستجو</button>
        </div>
      </div>
    </div>
  );
};

export default SearchForm;