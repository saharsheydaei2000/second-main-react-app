import React from 'react';
import SearchForm from "./Search"
import TableComponent from './Table';

const SearchTable = ({ searchboxItems, onSearch, onReset, columns, data }) => {
  return (
    <div className="search-component">
      <SearchForm searchboxItems={searchboxItems} onSearch={onSearch} onReset={onReset} />
      <TableComponent data={data} columns={columns} />
    </div>
  );
};

export default SearchTable;