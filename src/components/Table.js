import React from 'react';

const TableComponent = ({ data, columns }) => {
  return (
    <div className="table-component">
      <table border="1px solid gray" cellSpacing="0">
        <thead>
          <tr>
            {columns.map((column, index) => (
              <th key={index}>{column.headerTitle}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((rowData, rowIndex) => (
            <tr key={rowIndex}>
              {columns.map((column, colIndex) => {
                // If the column is for row numbers, render the row index
                if (column.headerTitle === 'ردیف') {
                  return <td key={colIndex}>{rowIndex + 1}</td>;
                }
                
                // Otherwise, render the regular data
                const value = column.render ? column.render(rowData) : rowData[column.dataFieldName];
                const cellContent = value || '_'; // Set default content if value is empty
                return <td key={colIndex}>{cellContent}</td>;
              })}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableComponent;